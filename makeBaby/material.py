import bpy


obj = bpy.context.active_object

img_out = bpy.data.images.load("C:\\Users\\shvr_ura\\Documents\\makeBaby\\black_skin\\color_skin.png", check_existing=True)


mat = bpy.data.materials.new('Skin')
mat.use_nodes = True
nodes = mat.node_tree.nodes
diffuse = nodes.new('ShaderNodeTexImage')
diffuse.location = (-600,200)

diffuse.image = bpy.data.images.load("C:/Users/shvr_ura/Documents/makeBaby/black_skin/black_skin_SPEC.png")

transform = nodes.new("ShaderNodeHueSaturation")




transform.location = (-300,250)

transform.color_ramp.elements[0].color = (0, 0, 0, 1)
transform.color_ramp.elements[0].position = 0

skin_color = (1, 1, 0.12, 1)

transform.color_ramp.elements[1].color = skin_color
transform.color_ramp.elements[1].position = 0.045


p_BSDF = nodes["Principled BSDF"]
mat.node_tree.links.new(diffuse.outputs['Color'], transform.inputs['Fac'])
mat.node_tree.links.new(transform.outputs['Color'], p_BSDF.inputs['Base Color'])

obj.data.materials.append(mat)


'''
diffuse.select = True
nodes.active = diffuse
p_BSDF.inputs[7].default_value = 0
p_BSDF.inputs[15].default_value = 1


# Assign it to object
if obj.data.materials:
    # assign to 1st material slot
    obj.data.materials[0] = material_glass
else:
    # no slots
    obj.data.materials.append(material_glass)
   
#Due to the presence of any multiple materials, it seems necessary to iterate on all the materials, and assign them a node + the image to bake.
for mat in obj.data.materials:

    mat.use_nodes = True #Here it is assumed that the materials have been created with nodes, otherwise it would not be possible to assign a node for the Bake, so this step is a bit useless
    nodes = mat.node_tree.nodes
    texture_node =nodes.new('ShaderNodeTexImage')
    texture_node.name = 'Bake_node'
    texture_node.select = True
    nodes.active = texture_node
    texture_node.image = img #Assign the image to the node
    
bpy.context.view_layer.objects.active = obj
bpy.ops.object.bake(type='DIFFUSE', save_mode='EXTERNAL')

img.save_render(filepath="C:\\Users\\shvr_ura\\Documents\\makeBaby\\black_skin\\export_skin.png")
    
#In the last step, we are going to delete the nodes we created earlier
for mat in obj.data.materials:
    for n in mat.node_tree.nodes:
        if n.name == 'Bake_node':
            mat.node_tree.nodes.remove(n)
'''