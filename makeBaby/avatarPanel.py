import bpy 

import numpy as np
from PIL import Image



preview_collections = {}

# Define the colors you want to create icons for
colors = [
    ("1", (0.91, 0.76, 0.65, 1)),  # Light skin
    ("2", (0.85, 0.68, 0.56, 1)),
    ("3", (0.76, 0.60, 0.50, 1)),
    ("4", (0.70, 0.52, 0.43, 1)),
    ("5", (0.61, 0.45, 0.36, 1)),
    ("6", (0.54, 0.36, 0.28, 1)),
    ("7", (0.45, 0.33, 0.26, 1)),
    ("8", (0.38, 0.27, 0.20, 1)),
    ("9", (0.33, 0.24, 0.18, 1)),
    ("10", (0.28, 0.20, 0.14, 1)),
    ("11", (0.23, 0.16, 0.12, 1)),  # Dark skin
    ("12", (0.19, 0.14, 0.10, 1)),
]

# Function to create an image of a specific color
def create_color_image(color, size=(32, 32)):
    # Create an array of the given color
    r, g, b, a = (int(c * 255) for c in color)
    data = np.full((size[1], size[0], 4), (r, g, b, a), dtype=np.uint8)
    
    # Convert to PIL image for easier manipulation if needed
    image = Image.fromarray(data, 'RGBA')
    
    return image

# Function to convert PIL image to a format Blender can use
def image_to_icon(image, name):
    # Convert PIL image to raw bytes
    raw_image = image.tobytes()
    
    # Create a Blender image
    img = bpy.data.images.new(name=name, width=image.width, height=image.height, alpha=True)
    img.pixels = [c / 255 for c in raw_image]  # Convert to Blender's pixel format
    
    # Save the image to a temporary file
    temp_filepath = bpy.app.tempdir + name + ".png"
    img.filepath_raw = temp_filepath
    img.file_format = 'PNG'
    img.save()

    return temp_filepath

def get_icon_value(icon_name: str) -> int:
    icon_items = bpy.types.UILayout.bl_rna.functions["prop"].parameters["icon"].enum_items.items()
    icon_dict = {tup[1].identifier : tup[1].value for tup in icon_items}

    return icon_dict[icon_name]

class HelperPanel(bpy.types.Panel):
    bl_label = "Requirements"
    bl_idname = "SCENE_PT_avatar_helper"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_context_mode = 'OBJECT'
    bl_options = {'DEFAULT_CLOSED'}
    
    bl_category = "Avatar"
    bl_parent_id = "SCENE_PT_avatar"
    

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        layout.label(text= "1. Download and install this Blender plugin : ")
        mpfb = layout.operator("mpfb.web_resource", icon="IMPORT", text="Download mpfb")
        mpfb.url = "https://static.makehumancommunity.org/mpfb/downloads.html"

        layout.label(text= "2. Download the following zip : ")
        
        default = layout.operator("mpfb.web_resource",  icon="IMPORT", text="Download makehuman_system_assets")
        default.url = "https://static.makehumancommunity.org/assets/assetpacks/makehuman_system_assets.html"
        glasses = layout.operator("mpfb.web_resource",  icon="IMPORT", text="Download glasses")
        glasses.url = "https://static.makehumancommunity.org/assets/assetpacks/glasses01.html"
        hair = layout.operator("mpfb.web_resource",  icon="IMPORT", text="Download facial hair")
        hair.url = "https://static.makehumancommunity.org/assets/assetpacks/bodyparts06.html"
        #suit = layout.operator("mpfb.web_resource",  icon="IMPORT", text="Download main suit")
        #suit.url = "http://www.makehumancommunity.org/clothes.html"
        #hijab = layout.operator("mpfb.web_resource",  icon="IMPORT", text="Download hijab")
        layout.label(text="please also load makebaby_assets.zip")

        layout.label(text="3. load all the zips one by one : ")
        load = layout.operator("mpfb.load_pack", icon ="FILE_FOLDER")


# Custom operator to set skin color
class SET_SKIN_COLOR_OT_Operator(bpy.types.Operator):
    bl_idname = "makebaby.set_skin_color"
    bl_label = "Set Skin Color"
    
    color: bpy.props.FloatVectorProperty(name="Color", subtype='COLOR', size=4)
    
    def execute(self, context):
        context.scene.makebaby_skin_color = self.color
        return {'FINISHED'}

class AvatarPanel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_label = "Customize Avatar"
    bl_idname = "SCENE_PT_avatar"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_context_mode = 'OBJECT'
    
    bl_category = "Avatar"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        pcoll = preview_collections["main"]


        
        layout.separator()
        lay = layout.column()
        split = lay.split(factor=0.5)
        left = split.column()
        right = split.column()

        ### New
        left.operator('makebaby.actions', icon="PLUS", text="New").action = 'NEW'
        right.operator('makebaby.actions', icon="TRASH", text="Delete").action = 'DELETE'
        
        layout.separator()
        lay1 = layout.column()
        split = lay1.split(factor=0.5)
        left1 = split.column()
        right1 = split.column()

        ### Body
        left1.label(text="Body Shape: ", icon="OUTLINER_OB_ARMATURE")
        left1.prop(scene, "makebaby_gender")
        left1.prop(scene, "makebaby_weight")
        left1.prop(scene, "makebaby_muscle")
        left1.prop(scene, "makebaby_proportions")
        left1.prop(scene, "makebaby_age")
	  
        ### Race
        right1.label(text="Face Shape: ", icon="WORLD")
        right1.template_color_picker(scene, "makebaby_race", value_slider=False, lock=False, lock_luminosity=False, cubic=False)

        layout.separator()
        lay2 = layout.column()
        split = lay2.split(factor=0.5)
        left2 = split.column()
        right2 = split.column()
    
        ### Color
        left2.label(text="Color: ", icon='EYEDROPPER')
        skin = left2.column()
        left2.prop(scene, "makebaby_eyes_color", text="Eyes ",  icon='HIDE_OFF')
        left2.prop(scene, "makebaby_hair_color", text="Hair ", icon='PARTICLEMODE')
        skin.prop(scene, "makebaby_skin_color", text="Skin ", icon='COMMUNITY')
        sks = skin.grid_flow(columns=4, even_columns=True, even_rows=True)
        for (name, color) in colors:
            box = sks.box()
            box.template_icon(icon_value=pcoll[f"color_icon_{name}"].icon_id, scale= 2.0)
            op = box.operator('makebaby.set_skin_color', text=name)
            op.color = color    

        left2.separator()
        ### Export
        export = left2.column()
        export.label(text="Export: ", icon='EXPORT')
        export.prop(context.scene, 'conf_path')
        export.operator('makebaby.actions', icon="EXPORT", text='Export').action = 'EXPORT'
  

        ### Hair Shape
        right2.label(text="Haircut: ", icon="PARTICLEMODE")
        hair = right2.grid_flow(columns=3, even_columns=True, even_rows=True)
        hairs = ["AFRO01", "BOB01", "BOB02", "BRAID01", "LONG01", "PONYTAIL01", "SHORT01", "SHORT02", "SHORT03", "SHORT04" ]
        for h in hairs :
            box = hair.box()
            box.template_icon(icon_value=pcoll[h.lower()].icon_id, scale= 2.0)
            box.operator('makebaby.actions', text=h.lower()).action = h + " HAIR"
        box = hair.box()
        box.template_icon(icon_value=get_icon_value("BLANK1"), scale= 2.0)
        box.operator('makebaby.actions', text='bald').action = "BALD HAIR"


        ### Glasses
        right2.label(text="Glasses: ", icon="CAMERA_STEREO")
        glass = right2.grid_flow(columns=3, even_columns=True, even_rows=True)
        glasses = ['spamrakuen_tbm_glasses_frames_01', 'toigo_round_glasses_leopard', 'kwnet_at_optical_glasses', 'spamrakuen_sagerfrogs_glasses_04']
        for g in glasses :
            box = glass.box()
            box.template_icon(icon_value=pcoll[g.lower()].icon_id, scale= 2.0)
            box.operator('makebaby.actions', text=g.lower()).action = g
        box = glass.box()
        box.template_icon(icon_value=get_icon_value("BLANK1"), scale= 2.0)
        box.operator('makebaby.actions', text='none').action = 'noglass'
 

        ### Facial Hair
        right2.label(text="Facial Hair: ", icon="PARTICLEMODE")
        facial = right2.grid_flow(columns=3, even_columns=True, even_rows=True)
        facials = ['culturalibre_dal_moustache', 'elvs_scruffy_beard1', 'grinsegold_full_beard', 'grinsegold_moustache']
        for f in facials :
            box = facial.box()
            box.template_icon(icon_value=pcoll[f.lower()].icon_id, scale= 2.0)
            box.operator('makebaby.actions', text=f.lower()).action = f
        box = facial.box()
        box.template_icon(icon_value=get_icon_value("BLANK1"), scale= 2.0)
        box.operator('makebaby.actions', text='none').action = 'nofacial'

         ### Eyebrows
        right2.label(text='Eyebrows: ', icon='HIDE_ON')
        brow = right2.grid_flow(columns=3, even_columns=True, even_rows=True)
        assets = ["EYEBROW001", "EYEBROW002", "EYEBROW006", 
                   "EYEBROW008", "EYEBROW009", "EYEBROW010", "EYEBROW012"]
        for asset in assets :
            box = brow.box()
            box.template_icon(icon_value=pcoll[asset.lower()].icon_id, scale= 2.0)
            box.operator('makebaby.actions', text=str(int(asset.lower().lstrip('eyebrow')))).action = asset

        ### Clothes 
        right2.label(text="Clothes: ", icon="MOD_CLOTH")
        clothe = right2.grid_flow(columns=3, even_columns=True, even_rows=True)
        clothes = ['hijab']
        for c in clothes :
            box = clothe.box()
            box.template_icon(icon_value=pcoll[c.lower()].icon_id, scale= 2.0)
            box.operator('makebaby.actions', text=c.lower()).action = c
        box = clothe.box()
        box.template_icon(icon_value=get_icon_value("BLANK1"), scale= 2.0)
        box.operator('makebaby.actions', text='none').action = 'nohat'
    

        



def register():
    # Note that preview collections returned by bpy.utils.previews
    # are regular py objects - you can use them to store custom data.
    import bpy.utils.previews
    pcoll = bpy.utils.previews.new()

    # path to the folder where the icon is
    import os
    BPY_HOME = os.path.abspath(bpy.utils.resource_path('USER'))
    DATAPATH = os.path.join(BPY_HOME,"mpfb", "data")

        
    for root, dirs, assets in os.walk(DATAPATH):
        for asset in assets:
            if asset.endswith('.thumb'):
                thumb_path = os.path.join(root, asset)
                name, extension = os.path.splitext(asset)
                pcoll.load(name, thumb_path, 'IMAGE')

    # Generate icons for each color
    for (name, color) in colors:
        image = create_color_image(color)
        icon_name = f"color_icon_{name}"
        temp_filepath = image_to_icon(image, icon_name)
        pcoll.load(icon_name, temp_filepath, 'IMAGE')

    # Store the preview collection
    preview_collections["main"] = pcoll


    

    bpy.utils.register_class(AvatarPanel) 
    bpy.utils.register_class(HelperPanel)
    bpy.utils.register_class(SET_SKIN_COLOR_OT_Operator)
    
    
    

def unregister():
    for pcoll in preview_collections.values():
        bpy.utils.previews.remove(pcoll)
    preview_collections.clear()

    #cleanup
    for (name, color) in colors:
        temp_filepath = bpy.app.tempdir + f"color_icon_{name}.png"
        os.remove(temp_filepath)

    bpy.utils.unregister_class(AvatarPanel)
    bpy.utils.register_class(HelperPanel) 
    bpy.utils.unregister_class(SET_SKIN_COLOR_OT_Operator)






