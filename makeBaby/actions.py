import bpy
import os
from selectUtils import select_basemesh, select_rig, colorise_material

import os
# OS = "linux"
# # OS = "windows"
# if OS == "linux":    
#     DATA_FOLDER = '/home/leanahtan/.config/blender/4.0/mpfb/data'
#     EXPORT_PATH = os.getcwd() + '/BA6/avatar/vr-hero/makeBaby'
#     BABY_PATH   = os.getcwd() + '/BA6/avatar/vr-hero/makeBaby'
#     SEPARATOR   = "/"
# elif OS == "windows":
#     DATA_FOLDER = "C:/Users/shvr_ura/AppData/Roaming/Blender Foundation/Blender/4.0/mpfb/data"
#     EXPORT_PATH = "C:/Users/shvr_ura/Documents/shvr-main/SHVRUnity/Assets/chars"
#     BABY_PATH   = "C:/Users/shvr_ura/Documents/makeBaby"
#     SEPARATOR   = "/"  


BPY_HOME = os.path.abspath(bpy.utils.resource_path('USER'))
DATAPATH = os.path.join(BPY_HOME,"mpfb", "data")


def handle_shape(self, context, shape, attribute_name, object_type, material_type, data_subfolder):
    select_basemesh()
    
    # Unload previous shape
    previous_shape = getattr(self, attribute_name)
    if previous_shape != 'none' and previous_shape != 'bald':
        unload_path = os.path.join(str(previous_shape), str(previous_shape) + '.mhclo')
        bpy.ops.mpfb.unload_library_clothes(filepath=unload_path)
        select_basemesh()

    # Load current shape
    if shape != 'none' and shape != 'bald':
        file_path = os.path.join(DATAPATH, data_subfolder, shape, shape + ".mhclo")
        bpy.ops.mpfb.load_library_clothes(filepath=file_path, object_type=object_type, material_type=material_type)

    # Register new shape for next unload
    setattr(self, attribute_name, shape)
    
    select_basemesh()


class Actions(bpy.types.Operator):
    bl_idname = 'makebaby.actions'
    bl_label = 'MakeBaby Actions'
    bl_description = 'Operator for the buttons of the UI'
    bl_options = {'REGISTER', 'UNDO'}
 
    action: bpy.props.EnumProperty(
        items=[
            ('NEW', 'create new avatar', 'create new avatar'),
            ('DELETE', 'delete current avatar', 'delete current avatar'),
            
            ('AFRO01 HAIR', 'change hair to affro', 'change hair to afro'),
            ('BOB01 HAIR', 'change hair to bob1', 'change hair to bob1'),
            ('BOB02 HAIR', 'change hair to bob2', 'change hair to bob2'),
            ('BRAID01 HAIR', 'change hair to braid', 'change hair to braid'),
            ('LONG01 HAIR', 'change hair to long', 'change hair to long'),
            ('PONYTAIL01 HAIR', 'change hair to ponytail', 'change hair to ponytail'),
            ('SHORT01 HAIR', 'change hair to short1', 'change hair to short1'),
            ('SHORT02 HAIR', 'change hair to short2', 'change hair to short2'),
            ('SHORT03 HAIR', 'change hair to short3', 'change hair to short3'),
            ('SHORT04 HAIR', 'change hair to short4', 'change hair to short4'),
            ('BALD HAIR', 'change hair to bald', 'change hair to bald'),

            ('EYEBROW001', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW002', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW003', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW004', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW005', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW006', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW007', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW008', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW009', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW010', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW011', 'change eyebrow', 'change eyebrow'),
            ('EYEBROW012', 'change eyebrow', 'change eyebrow'),

            ('spamrakuen_tbm_glasses_frames_01', 'glasses', 'glasses'),
            ('toigo_round_glasses_leopard', 'glasses', 'glasses'),
            ('kwnet_at_optical_glasses', 'glasses', 'glasses'),
            ('spamrakuen_sagerfrogs_glasses_04', 'glasses', 'glasses'),
            ('noglass', 'glasses', 'glasses'),

            ('culturalibre_dal_moustache', 'facial', 'facial'),
            ('elvs_scruffy_beard1', 'facial', 'facial'),
            ('grinsegold_full_beard', 'facial', 'facial'),
            ('grinsegold_moustache', 'facial', 'facial'),
            ('nofacial', 'facial', 'facial'),

            ('hijab', 'hat', 'hat'),
            ('nohat', 'hat', 'hat'),

            ('EXPORT', 'Export FBX', 'Export FBX'),
        ]
    )

    glasses_shape: bpy.props.EnumProperty(
        items=[
            ('spamrakuen_tbm_glasses_frames_01', 'spamrakuen_tbm_glasses_frames_01', 'spamrakuen_tbm_glasses_frames_01'),
            ('toigo_round_glasses_leopard', 'toigo_round_glasses_leopard', 'toigo_round_glasses_leopard'),
            ('kwnet_at_optical_glasses', 'kwnet_at_optical_glasses', 'kwnet_at_optical_glasses'),
            ('spamrakuen_sagerfrogs_glasses_04', 'spamrakuen_sagerfrogs_glasses_04', 'spamrakuen_sagerfrogs_glasses_04'),
            ('none', 'none', 'none')
        ]
    )
                  
    hair_shape: bpy.props.EnumProperty(
        items=[
            ('afro01', 'afro01', 'afro01'),
            ('bob01', 'bob01', 'bob01'),
            ('bob02', 'bob02', 'bob02'),
            ('braid01', 'braid01', 'braid01'),
            ('long01', 'long01', 'long01'),
            ('ponytail01', 'ponytail01', 'ponytail01'),
            ('short01', 'short01', 'short01'),
            ('short02', 'short02', 'short02'),
            ('short03', 'short03', 'short03'),
            ('short04', 'short04', 'short04'),
            ('bald', 'bald', 'bald'),
        ]
    )

    eyebrow_shape: bpy.props.EnumProperty(
        items=[
            ('eyebrow001', 'eyebrow001', 'eyebrow001'),
            ('eyebrow002', 'eyebrow002', 'eyebrow002'),
            ('eyebrow003', 'eyebrow003', 'eyebrow003'),
            ('eyebrow004', 'eyebrow004', 'eyebrow004'),
            ('eyebrow005', 'eyebrow005', 'eyebrow005'),
            ('eyebrow006', 'eyebrow006', 'eyebrow006'),
            ('eyebrow007', 'eyebrow007', 'eyebrow007'),
            ('eyebrow008', 'eyebrow008', 'eyebrow008'),
            ('eyebrow009', 'eyebrow009', 'eyebrow009'),
            ('eyebrow010', 'eyebrow010', 'eyebrow010'),
            ('eyebrow011', 'eyebrow011', 'eyebrow011'),
            ('eyebrow012', 'eyebrow012', 'eyebrow012'),
        ]
    )

    facial_hair: bpy.props.EnumProperty(
        items=[
            ('culturalibre_dal_moustache', 'culturalibre_dal_moustache', 'culturalibre_dal_moustache'),
            ('elvs_scruffy_beard1', 'elvs_scruffy_beard1', 'elvs_scruffy_beard1'),
            ('grinsegold_full_beard', 'grinsegold_full_beard', 'grinsegold_full_beard'),
            ('grinsegold_moustache', 'grinsegold_moustache', 'grinsegold_moustache'),
            ('none', 'none', 'none')
        ]
    )

    hat_shape: bpy.props.EnumProperty(
        items=[
            ('hijab', 'hijab', 'hijab'),
            ('none', 'none', 'none')
        ]
    )


 
    
    def execute(self, context):
        if self.action == 'EXPORT':
            self.export(context=context, export_folder = context.scene.conf_path)
        elif self.action == 'NEW':
            self.new(self=self, context=context)
        elif self.action == 'DELETE':
            self.delete(self=self, context=context)
        # HAIR
        elif self.action == 'AFRO01 HAIR':
            self.hair(self=self, context=context, shape='afro01')
        elif self.action == 'BOB01 HAIR':
            self.hair(self=self, context=context, shape='bob01')
        elif self.action == 'BOB02 HAIR':
            self.hair(self=self, context=context, shape='bob02')  
        elif self.action == 'BRAID01 HAIR':
            self.hair(self=self, context=context, shape='braid01') 
        elif self.action == 'LONG01 HAIR':
            self.hair(self=self, context=context, shape='long01') 
        elif self.action == 'PONYTAIL HAIR':
            self.hair(self=self, context=context, shape='ponytail01') 
        elif self.action == 'SHORT01 HAIR':
            self.hair(self=self, context=context, shape='short01') 
        elif self.action == 'SHORT02 HAIR':
            self.hair(self=self, context=context, shape='short02') 
        elif self.action == 'SHORT03 HAIR':
            self.hair(self=self, context=context, shape='short03') 
        elif self.action == 'SHORT04 HAIR':
            self.hair(self=self, context=context, shape='short04')   
        elif self.action == 'BALD HAIR':
            self.hair(self=self, context=context, shape='bald')
        # EYEBROWS
        elif self.action == 'EYEBROW001':
            self.eyebrowes(self=self, context=context, shape='eyebrow001')
        elif self.action == 'EYEBROW002':
            self.eyebrowes(self=self, context=context, shape='eyebrow002')   
        elif self.action == 'EYEBROW003':
            self.eyebrowes(self=self, context=context, shape='eyebrow003')   
        elif self.action == 'EYEBROW004':
            self.eyebrowes(self=self, context=context, shape='eyebrow004')   
        elif self.action == 'EYEBROW005':
            self.eyebrowes(self=self, context=context, shape='eyebrow005')   
        elif self.action == 'EYEBROW006':
            self.eyebrowes(self=self, context=context, shape='eyebrow006')   
        elif self.action == 'EYEBROW007':
            self.eyebrowes(self=self, context=context, shape='eyebrow007')   
        elif self.action == 'EYEBROW008':
            self.eyebrowes(self=self, context=context, shape='eyebrow008')   
        elif self.action == 'EYEBROW009':
            self.eyebrowes(self=self, context=context, shape='eyebrow009')   
        elif self.action == 'EYEBROW010':
            self.eyebrowes(self=self, context=context, shape='eyebrow010')
        elif self.action == 'EYEBROW011':
            self.eyebrowes(self=self, context=context, shape='eyebrow011') 
        elif self.action == 'EYEBROW012':
            self.eyebrowes(self=self, context=context, shape='eyebrow012')    
        # GLASSES        
        elif self.action == 'spamrakuen_tbm_glasses_frames_01':
            self.glasses(self=self, context=context, shape='spamrakuen_tbm_glasses_frames_01')
        elif self.action == 'toigo_round_glasses_leopard':
            self.glasses(self=self, context=context, shape='toigo_round_glasses_leopard')
        elif self.action == 'kwnet_at_optical_glasses':
            self.glasses(self=self, context=context, shape='kwnet_at_optical_glasses')
        elif self.action == 'spamrakuen_sagerfrogs_glasses_04':
            self.glasses(self=self, context=context, shape='spamrakuen_sagerfrogs_glasses_04')
        elif self.action == 'nogalss':
            self.glasses(self=self, context=context, shape='none')
        # FACIAL HAIR
        elif self.action == 'culturalibre_dal_moustache':
            self.facial(self=self, context=context, shape='culturalibre_dal_moustache')
        elif self.action == 'elvs_scruffy_beard1':
            self.facial(self=self, context=context, shape='elvs_scruffy_beard1')
        elif self.action == 'grinsegold_full_beard':
            self.facial(self=self, context=context, shape='grinsegold_full_beard')
        elif self.action == 'grinsegold_moustache':
            self.facial(self=self, context=context, shape='grinsegold_moustache')
        elif self.action == 'nofacial':
            self.facial(self=self, context=context, shape='none')
        # HATS
        elif self.action == 'hijab':
            self.hats(self=self, context=context, shape='hijab')
        elif self.action == 'nohat':
            self.hats(self=self, context=context, shape='none')
        return {'FINISHED'}
    
    
    def export(self, context, export_folder):
        # Select everything relevant
        rig = select_rig()
        for obj in rig.children:
            obj.select_set(True)

        # Ensure there are selected objects
        selected_objects = bpy.context.selected_objects
        if not selected_objects:
            return

        # Ensure the export folder exists
        if not os.path.exists(export_folder):
            os.makedirs(export_folder)

        # Set bake settings
        bpy.context.scene.render.engine = 'CYCLES'
        bpy.context.scene.cycles.samples = 32
        bpy.context.scene.render.threads_mode = 'AUTO'
        bpy.context.scene.cycles.bake_type = 'DIFFUSE'
        bpy.context.scene.render.bake.use_pass_direct = False
        bpy.context.scene.render.bake.use_pass_indirect = False

        for obj in selected_objects:
            bpy.ops.object.select_all(action='DESELECT')
            if obj.type != 'MESH':
                continue

            # Create a new image to bake to
            image_name = f"{obj.name}_Baked"
            width, height = 1024, 1024
            image = bpy.data.images.new(image_name, width=width, height=height)

            # Create a new texture node in the active material
            if not obj.data.materials:
                continue
            
            mat = obj.data.materials[0]
            mat.use_nodes = True
            nodes = mat.node_tree.nodes
            texture_node = nodes.new(type='ShaderNodeTexImage')
            texture_node.image = image

            # Set the bake target to the new texture node   
            bpy.context.view_layer.objects.active = obj
            obj.select_set(True)
            mat.node_tree.nodes.active = texture_node

            # Bake the material
            bpy.ops.object.bake(type='DIFFUSE')

            # Save the image to a file
            output_path = os.path.join(export_folder, f"{obj.name}.png")
            image.filepath_raw = output_path
            image.file_format = 'PNG'
            image.save()

        rig = select_rig()
        for obj in rig.children:
            obj.select_set(True)
            
       
        bpy.ops.export_scene.fbx(
            #set to assets folder of Unity project
            filepath            = export_folder + '/avatar_export.fbx',
            check_existing      = True,

            use_selection       = True,

            path_mode           = 'COPY', 
            embed_textures      = True, 

            axis_forward        = '-Z',
            axis_up             = 'Y',

            global_scale        = 1.0,
            
            #armature_nodetype  = 'ROOT',
            
            primary_bone_axis   = 'Z', 
            secondary_bone_axis = '-X', 
            
                
        )

        self.report({'INFO'}, f"Successful export at {export_folder} ")

    def report(self, type, message):
        self.layout.label(text=message)
     
    @staticmethod
    def new(self, context):

      



        # settings for Unity
        bpy.context.scene.MPFB_ASLS_skin_type = 'MAKESKIN'
        bpy.context.scene.MPFB_ASLS_material_instances = False
        bpy.context.scene.MPFB_ASLS_procedural_eyes = False

        bpy.ops.mpfb.create_human()

        # set rig
        bpy.context.scene.MPFB_ADR_standard_rig = 'game_engine'
        bpy.ops.mpfb.add_standard_rig()
        select_basemesh()

        # set default values in UI
        context.scene.makebaby_gender      = 3 
        context.scene.makebaby_weight      = 3 
        context.scene.makebaby_muscle      = 3 
        context.scene.makebaby_proportions = 3 
        context.scene.makebaby_race  = (0.001, 0.001, 0.001, 1)
        context.scene.makebaby_age         = 1
        context.scene.makebaby_skin_color = (0.539368, 0.380481, 0.298517, 1)
        context.scene.makebaby_eyes_color = (0.0311476, 0.139338, 0.0120831, 1)
        context.scene.makebaby_hair_color = (0.0217573, 0.00310486, 0.00122452, 1)
     

        # set defaults
        self.eyebrow_shape = "eyebrow001"
        self.glasses_shape = "none"
        self.facial_hair = "none"
        self.hat_shape = 'none'

        # 1. Load the items
        data = [
            (['eyelashes', 'eyelashes01'], 'eyelashes01.mhclo', 'Eyelashes', 'MAKESKIN', ''),
            (['eyebrows', 'eyebrow001'], 'eyebrow001.mhclo', 'Eyebrows', 'MAKESKIN', ''),
            (['clothes', 'shoes04'], 'shoes04.mhclo', 'Clothes' , 'MAKESKIN', 'shoes04.mhmat'),
        ]
        for folders, file_name, obj_type, mat_type, mat_file in data:
            data_folder_path = os.path.join(DATAPATH, *folders)
            file_path = os.path.join(data_folder_path, file_name)
            if obj_type == 'Skin' :
                bpy.ops.mpfb.load_library_skin(filepath=file_path)
            else:
                bpy.ops.mpfb.load_library_clothes(filepath=file_path, object_type=obj_type, material_type=mat_type)
            if mat_file:
                bpy.context.scene.MPFB_ALTM_available_materials = mat_file
                bpy.ops.mpfb.load_library_material()
 
        # 2. load the suit
        bpy.ops.mpfb.load_library_clothes(
            filepath=os.path.join(DATAPATH, 'clothes', 'main_suit', 'main_suit.mhclo'),
            object_type = 'Clothes',
            material_type = 'MAKESKIN'
        )
        select_basemesh()

        # 3. add the eyes
        bpy.ops.mpfb.load_library_clothes(
            filepath = os.path.join(DATAPATH, 'eyes', 'low-poly', 'low-poly.mhclo'),
            object_type = 'Eyes',
            material_type = 'GAMEENGINE',
        )
        for obj in bpy.context.selected_objects:
            obj.name = "Human.eyes"
        eye_image = os.path.join(DATAPATH, 'eyes', 'materials', 'blue_eye.png')
        obj = bpy.context.active_object
        mat = bpy.data.materials.get('Eyes')
        if mat is None:
            mat = bpy.data.materials.new(name='Eyes')
        mat.use_nodes = True
        nodes = mat.node_tree.nodes
        diffuse = nodes.new('ShaderNodeTexImage')
        diffuse.location = (-600, -100)
        diffuse.image = bpy.data.images.load(eye_image)
        hsv = nodes.new("ShaderNodeHueSaturation")
        hsv.location = (-300,250)
        addHue = nodes.new("ShaderNodeMath")
        addHue.location = (-570, 300)
        addHue.inputs[1].default_value = 0.9
        property_color = nodes.new("ShaderNodeSeparateColor")
        property_color.location = (-700, 200)
        property_color.mode = 'HSV'
        property_color.inputs["Color"].default_value = context.scene.makebaby_eyes_color
        value = nodes.new("ShaderNodeSeparateColor")
        value.location = (-1200, 400)
        value.mode = 'HSV'
        shift_up = nodes.new("ShaderNodeMath")
        shift_up.location = (-1100, 400)
        shift_up.inputs[1].default_value = 0.8
        truncate = nodes.new("ShaderNodeMath")
        truncate.location = (-1000, 400)
        truncate.operation = 'TRUNC'
        bpy.data.materials["Eyes"].node_tree.nodes["Math"].operation = 'MULTIPLY'
        apply_value_mask = nodes.new("ShaderNodeMath")
        apply_value_mask.location = (-900, 300)
        apply_value_mask.operation = "MAXIMUM"
        p_BSDF = nodes["Principled BSDF"]
        mat.node_tree.links.new(diffuse.outputs['Color'], hsv.inputs['Color'])
        mat.node_tree.links.new(diffuse.outputs['Color'], value.inputs['Color'])
        mat.node_tree.links.new(value.outputs[2], shift_up.inputs[0])
        mat.node_tree.links.new(shift_up.outputs['Value'], truncate.inputs['Value'])
        mat.node_tree.links.new(truncate.outputs["Value"], apply_value_mask.inputs[1])
        mat.node_tree.links.new(property_color.outputs[2], apply_value_mask.inputs[0])
        mat.node_tree.links.new(apply_value_mask.outputs["Value"], hsv.inputs["Value"])
        mat.node_tree.links.new(property_color.outputs[0], addHue.inputs[0])
        mat.node_tree.links.new(addHue.outputs[0], hsv.inputs['Hue'])
        mat.node_tree.links.new(property_color.outputs[1], hsv.inputs['Saturation'])
        mat.node_tree.links.new(hsv.outputs['Color'], p_BSDF.inputs['Base Color'])
        if obj.data.materials:
            obj.data.materials[0] = mat
        else:
            obj.data.materials.append(mat)
        select_basemesh()

        # 4. add hair material
        self.hair_shape = 'short01'
        bpy.ops.mpfb.load_library_clothes(
            filepath = os.path.join(DATAPATH, 'hair', 'short01', 'short01.mhclo'),
            object_type = 'Hair',
            material_type = 'MAKESKIN',
        )
        mat = bpy.data.materials.get("Human." + self.hair_shape)
        mat.node_tree.nodes["diffuseIntensity"].inputs["Fac"].default_value = 0.816667
        mat.node_tree.nodes["diffuseIntensity"].inputs[1].default_value = context.scene.makebaby_hair_color
        mat.name = "Hair"
        for obj in bpy.context.selected_objects:
            obj.name = "Human.hair"
        select_basemesh()
        
        # 5. add skin material 'Skin'
        skin_image = os.path.join(DATAPATH, 'skins', 'young_caucasian_female', 'young_lightskinned_female_diffuse.png')
        colorise_material(context, 'Skin', skin_image, context.scene.makebaby_skin_color, 0.44)

    @staticmethod
    def delete(self, context):
        rig = select_rig()
        for obj in rig.children:
            print(obj)
            obj.select_set(True)

        if bpy.data.materials.get('Skin'):
            skin = bpy.data.materials['Skin']
            bpy.data.materials.remove(skin)

        if bpy.data.materials.get('Eyes'):
            eyes = bpy.data.materials['Eyes']
            bpy.data.materials.remove(eyes)

        if bpy.data.materials.get('Hair'):
            hair = bpy.data.materials['Hair']
            bpy.data.materials.remove(hair)

        bpy.ops.object.delete(use_global=False)

    @staticmethod
    def eyebrowes(self, context, shape):
        handle_shape(self, context, shape, 'eyebrow_shape', "Eyebrows", "MAKESKIN", 'eyebrows')
    
    @staticmethod
    def glasses(self, context, shape):
        handle_shape(self, context, shape, 'glasses_shape', "Clothes", "MAKESKIN", 'clothes')
    
    @staticmethod
    def facial(self, context, shape):
        handle_shape(self, context, shape, 'facial_hair', "Clothes", "MAKESKIN", 'clothes')

    @staticmethod
    def hats(self, context, shape):
        handle_shape(self, context, shape, 'hat_shape', "Clothes", "MAKESKIN", 'clothes')
    
    @staticmethod
    def hair(self, context, shape):
        pass
    #     select_basemesh()
    
    #     # Unload previous shape
    #     previous_shape = getattr(self, attribute_name)
    #     if previous_shape != 'none' and previous_shape != 'bald':
    #         unload_path = os.path.join(str(previous_shape), str(previous_shape) + '.mhclo')
    #         bpy.ops.mpfb.unload_library_clothes(filepath=unload_path)
    #         select_basemesh()

    #         # Deleting previous material for hair
    #         if attribute_name == 'hair_shape' and bpy.data.materials.get('Human.' + previous_shape):
    #             hair = bpy.data.materials['Human.' + previous_shape]
    #             bpy.data.materials.remove(hair)

    #     # Execute pre-load callback if provided
    #     if pre_load_callback:
    #         pre_load_callback(self, context, shape)

    #     # Load current shape
    #     if shape != 'none' and shape != 'bald':
    #         file_path = os.path.join(DATAPATH, data_subfolder, shape, shape + ".mhclo")
    #         bpy.ops.mpfb.load_library_clothes(filepath=file_path, object_type=object_type, material_type=material_type)

    #         # Execute post-load callback if provided
    #         if post_load_callback:
    #             post_load_callback(self, context, shape)
    
    # # Register new shape for next unload
    # setattr(self, attribute_name, shape)
    
    # select_basemesh()
    # select_basemesh()
    #     def pre_load(self, context, shape):
    #         # Deleting previous material
    #         if bpy.data.materials.get('Hair'):
    #             hair = bpy.data.materials['Hair']
    #             bpy.data.materials.remove(hair)

    #     def post_load(self, context, shape):
    #         mat = bpy.data.materials.get("Human." + shape)
    #         if mat:
    #             mat.node_tree.nodes["diffuseIntensity"].inputs["Fac"].default_value = 0.816667
    #             mat.node_tree.nodes["diffuseIntensity"].inputs[1].default_value = context.scene.makebaby_hair_color
    #             mat.name = "Hair"

    #     handle_shape(self, context, shape, 'hair_shape', "Hair", "MAKESKIN", 'hair', pre_load, post_load)


def register():
    bpy.utils.register_class(Actions)

def unregister():
    bpy.utils.unregister_class(Actions)