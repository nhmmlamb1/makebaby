import bpy
from bpy.types import (Panel, Operator)

# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

class SimpleOperator(Operator):
    """Print object name in Console"""
    bl_idname = "object.simple_operator"
    bl_label = "Simple Object Operator"

    def execute(self, context):
        print (context.object)
        return {'FINISHED'}

# ------------------------------------------------------------------------
#    Panel in Object Mode
# ------------------------------------------------------------------------

class OBJECT_PT_CustomPanel(Panel):
    bl_idname = "object.custom_panel"
    bl_label = "My Panel"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "UI"
    bl_category = "Tools"
    bl_context = "objectmode"   


    @classmethod
    def poll(self,context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object

        layout.label(text="Properties:")

        col = layout.column(align=True)
        row = col.row(align=True)
        row.prop(obj, "show_name", toggle=True, icon="FILE_FONT")
        row.prop(obj, "show_wire", toggle=True, text="Wireframe", icon="SHADING_WIRE")
        col.prop(obj, "show_all_edges", toggle=True, text="Show all Edges", icon="MOD_EDGESPLIT")
        layout.separator()

        layout.label(text="Operators:")

        col = layout.column(align=True)
        col.operator(SimpleOperator.bl_idname, text="Execute Something", icon="CONSOLE")
        col.operator(SimpleOperator.bl_idname, text="Execute Something Else", icon="CONSOLE")

        layout.separator()

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_class(OBJECT_PT_CustomPanel)
    bpy.utils.register_class(SimpleOperator)

def unregister():
    bpy.utils.unregister_class(OBJECT_PT_CustomPanel)
    bpy.utils.unregister_class(SimpleOperator)

if __name__ == "__main__":
    register()
    
    
    
    
def begin_scene():

    # prepare viewport    
    bpy.context.space_data.shading.type = 'MATERIAL'
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=False)
    # add camera
    bpy.context.scene.render.resolution_x = 1080
    bpy.ops.object.camera_add(enter_editmode=False,location=(0.24722, -2.6153, 1.428),rotation=(77.684, 0, 3.8333),scale=(1, 1, 1))




    bpy.context.scene.MPFB_NH_phenotype_gender = 'male'
    bpy.context.scene.MPFB_NH_phenotype_influence = 0.37
    bpy.ops.mpfb.create_human()


    # the first time loqd the assets pack :
    bpy.ops.mpfb.load_pack(filepath="C:\\Users\\shvr_ura\\Documents\\blenderAddons\\makehuman_system_assets_cc0.zip")




    # disable procedural eyes and material instances, set skin type
    bpy.context.scene.MPFB_ASLS_skin_type = 'MAKESKIN'
    bpy.context.scene.MPFB_ASLS_material_instances = False
    bpy.context.scene.MPFB_ASLS_procedural_eyes = False
    # change values
    bpy.context.scene.MPFB_NH_phenotype_gender = 'male'
    bpy.context.scene.MPFB_NH_phenotype_influence = 0.37
    # make human
    bpy.ops.mpfb.create_human()
    # adjust phenotype
    bpy.context.scene.mpfb_macropanel_gender = 0.57
    bpy.context.scene.mpfb_macropanel_muscle = 0.65
    bpy.context.scene.mpfb_macropanel_age = 0.95
    bpy.context.scene.mpfb_macropanel_proportions = 0.89
    bpy.context.scene.mpfb_macropanel_asian = 2.98023e-08
    bpy.context.scene.mpfb_macropanel_african = 1
    # tweak shape 
    bpy.context.scene.chin_chin_bones_decr_incr = 0.21
    # set game engine rig
    bpy.context.scene.MPFB_ADR_standard_rig = 'game_engine'
    # click add rig
    bpy.ops.object.armature_add(enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.posemode_toggle()
    bpy.ops.object.posemode_toggle()
    bpy.ops.object.modifier_move_up(modifier="Armature")
    bpy.ops.object.posemode_toggle()
    bpy.ops.object.posemode_toggle()
    bpy.ops.mpfb.add_standard_rig()
    # select character
    # ?
    # add low poly eyes
    bpy.ops.wm.obj_import(filepath="C:\\Users\\shvr_ura\\AppData\\Roaming\\Blender Foundation\\Blender\\4.0\\mpfb\\data\\eyes\\low-poly\\low-poly.obj", use_split_objects=False, use_split_groups=False)
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
    bpy.ops.object.shade_smooth()
    bpy.ops.mpfb.load_library_clothes(filepath="C:\\Users\\shvr_ura\\AppData\\Roaming\\Blender Foundation\\Blender\\4.0\\mpfb\\data\\eyes\\low-poly\\low-poly.mhclo", object_type="Eyes", material_type="MAKESKIN")
    # select character
    # ?
    # add casual suit
    bpy.ops.wm.obj_import(filepath="C:\\Users\\shvr_ura\\AppData\\Roaming\\Blender Foundation\\Blender\\4.0\\mpfb\\data\\clothes\\male_casualsuit02\\male_casualsuit02.obj", use_split_objects=False, use_split_groups=False)
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
    bpy.ops.object.shade_smooth()
    bpy.ops.mpfb.load_library_clothes(filepath="C:\\Users\\shvr_ura\\AppData\\Roaming\\Blender Foundation\\Blender\\4.0\\mpfb\\data\\clothes\\male_casualsuit02\\male_casualsuit02.mhclo", object_type="Clothes", material_type="MAKESKIN")
    # add skin
    bpy.ops.mpfb.load_library_skin(filepath="C:\\Users\\shvr_ura\\AppData\\Roaming\\Blender Foundation\\Blender\\4.0\\mpfb\\data\\skins\\middleage_asian_female\\middleage_asian_female.mhmat")
