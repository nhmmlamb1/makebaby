import bpy


# this is useless now that I have a default file with the correct room setup

def main(context):
    #switch cam
    if context.region_data.view_perspective in  {'PERSP','ORTHO'}: 
        context.region_data.view_perspective = 'CAMERA'
    else:                            
        context.region_data.view_perspective = 'PERSP'

    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.object.select_by_type(type='MESH')
    bpy.ops.object.select_by_type(type='LIGHT')
    bpy.ops.object.delete()


class CameraSetup(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.camera_setup"
    bl_label = "Camera Setup"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        main(context)
        return {'FINISHED'}


def menu_func(self, context):
    self.layout.operator(CameraSetup.bl_idname, text=CameraSetup.bl_label)


# Register and add to the "object" menu 0
# (required to also use F3 search "Simple Object Operator" for quick access).
def register():
    bpy.utils.register_class(CameraSetup)
    bpy.types.VIEW3D_MT_object.append(menu_func)


def unregister():
    bpy.utils.unregister_class(CameraSetup)
    bpy.types.VIEW3D_MT_object.remove(menu_func)


if __name__ == "__main__":
    register()
