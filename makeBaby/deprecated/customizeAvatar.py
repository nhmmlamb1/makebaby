import bpy
import os

from math import sin, pi, radians

# OS = "linux"
OS = "windows"
if OS == "linux":
    DATA_FOLDER = '/home/leanahtan/.config/blender/4.0/mpfb/data'
    EXPORT_PATH = os.getcwd() + '/BA6/avatar/vr-hero/makeBaby'
    SEPARATOR = "/"
elif OS == "windows":
    DATA_FOLDER = "C:/Users/shvr_ura/AppData/Roaming/Blender Foundation/Blender/4.0/mpfb/data"
    EXPORT_PATH = "C:/Users/shvr_ura/Documents/shvr-main/SHVRUnity/Assets/chars"
    SEPARATOR = "/"
    


def select_basemesh():
    basemesh = bpy.context.scene.objects["Human"]       # Get the object
    bpy.ops.object.select_all(action='DESELECT')        # Deselect all objects
    bpy.context.view_layer.objects.active = basemesh    # Make the cube the active object 
    basemesh.select_set(True)
    return basemesh
    

def select_rig():
    rig = bpy.context.scene.objects["Human.rig"]              # Get the object
    bpy.ops.object.select_all(action='DESELECT')        # Deselect all objects
    bpy.context.view_layer.objects.active = rig         # Make the cube the active object 
    rig.select_set(True)
    return rig

def update_body_shape(self, context):
    
    def smoothstep(x):
        return x * x * (3.0 - 2.0 * x)

    shape = context.scene.makebaby_bodyshape / 10
    
    context.scene.mpfb_macropanel_gender = shape
    context.scene.mpfb_macropanel_cupsize = 0.75 - 0.25*smoothstep(shape)
    
    adapt_height(context)
    
    bpy.ops.mpfb.refit_human()

    
def update_morpho(self, context):
    morpho = context.scene.makebaby_morphology / 10
    
    context.scene.mpfb_macropanel_muscle      = 0.5 + 0.5*sin(2*pi*morpho)
    context.scene.mpfb_macropanel_proportions = 0.5 + 0.5*sin(8*pi*morpho)
    context.scene.mpfb_macropanel_weight      = 0.5 + 0.5*sin(4*pi*morpho)
    
 
    
    bpy.ops.mpfb.refit_human()
 

def adapt_height(context):
    gender = context.scene.mpfb_macropanel_gender
    asian = context.scene.mpfb_macropanel_asian
    caucasian = context.scene.mpfb_macropanel_caucasian
    
    # def quad(gain, infl, x):
    #     b = 2*gain*infl / (2*infl + 1)
    #     a = gain - b
    #     return a*pow(x, 2) + b*x 
    
    context.scene.mpfb_macropanel_height = (0.5
         - 0.150*gender
         + 0.060*caucasian
         + 0.138*asian
    )
 
normalizing = False   
def normalise_race(context):
    
    global normalizing
    if normalizing:
        return
    
    normalizing = True
    
    total_sum = (
        bpy.context.scene.makebaby_african
        + bpy.context.scene.makebaby_asian
        + bpy.context.scene.makebaby_caucasian
    )
    
    if total_sum != 0:
        bpy.context.scene.makebaby_african /= total_sum
        bpy.context.scene.makebaby_asian /= total_sum
        bpy.context.scene.makebaby_caucasian /= total_sum
    else:
        bpy.context.scene.makebaby_african = 1/3
        bpy.context.scene.makebaby_asian = 1/3
        bpy.context.scene.makebaby_caucasian = 1/3
        
    normalizing = False

def update_african(self, context):
    context.scene.mpfb_macropanel_african = context.scene.makebaby_african
    normalise_race(context)
    bpy.ops.mpfb.refit_human()

def update_asian(self, context):
    asian = context.scene.makebaby_asian
    context.scene.mpfb_macropanel_asian = asian
    adapt_height(context)    
    normalise_race(context)
    bpy.ops.mpfb.refit_human()

def update_caucasian(self, context):
    context.scene.mpfb_macropanel_caucasian = context.scene.makebaby_caucasian
    adapt_height(context)
    normalise_race(context)
    bpy.ops.mpfb.refit_human()


bpy.types.Scene.makebaby_bodyshape = bpy.props.FloatProperty(name="Shape", min=0, max=10, default=5, update=update_body_shape)
bpy.types.Scene.makebaby_morphology = bpy.props.FloatProperty(name="Morphology", min=0, max=10, default=5, update=update_morpho)

bpy.types.Scene.makebaby_african   = bpy.props.FloatProperty(name="African"  , min=0, max=1, default=1/3, update=update_african)
bpy.types.Scene.makebaby_asian     = bpy.props.FloatProperty(name="Asian"    , min=0, max=1, default=1/3, update=update_asian)
bpy.types.Scene.makebaby_caucasian = bpy.props.FloatProperty(name="Caucasian", min=0, max=1, default=1/3, update=update_caucasian)



""""
gender
age
muscle
weight
height
proportions

cupsize
firmness
 
african
asian
caucasian
"""

class AvatarPanel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_label = "Customize Avatar"
    bl_idname = "SCENE_PT_avatar"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_context_mode = 'OBJECT'
    
    bl_category = "Avatar"

    def test():
        print("stuff")

    def draw(self, context):
        layout = self.layout

        scene = context.scene

        
        # Create two columns, by using a split layout.
        # split = layout.split()

        # First column
        # col = split.column()
        # col.label(text="Column One:")
        # col.prop(scene, "frame_end")
        # col.prop(scene, "frame_start")

        # Second column, aligned
        # col = split.column(align=True)
        # col.label(text="Column Two:")
        # col.prop(scene, "frame_start")
        # col.prop(scene, "frame_end")

        # Big render button
        # layout.label(text="Big Button:")
        # row = layout.row()
        # row.scale_y = 3.0
        # row.operator("mpfb.create_human")
        
        #model.prunehuman
        #model.refithuman
        #box.prop(mpfb_macropanel_
        
        ### New Avatar + Delete Avatar
        create = layout.row()
        create.operator('makebaby.actions', icon="PLUS", text="New").action = 'NEW'
        create.operator('makebaby.actions', icon="TRASH", text="Delete").action = 'DELETE'
        
        ### Body
        layout.label(text="Body : ", icon="OUTLINER_OB_ARMATURE")
        body = layout.column(align=True)
        body.prop(scene, "makebaby_bodyshape")
        body.prop(scene, "makebaby_morphology")
                
	  
        ### Race
        layout.label(text="Ethnicity: ", icon="WORLD")
        race = layout.row(align=True)        
        race.prop(scene, "makebaby_african")
        race.prop(scene, "makebaby_asian")
        race.prop(scene, "makebaby_caucasian")

        ### Skin
        layout.label(text="Skin: ", icon='COMMUNITY')
        skin = layout.row()
        skin.operator('makebaby.actions', icon="SEQUENCE_COLOR_07", text='').action = 'CAUCASIAN SKIN'
        skin.operator('makebaby.actions', icon="SEQUENCE_COLOR_02", text='').action = 'MIDLETON SKIN'
        skin.operator('makebaby.actions', icon="SEQUENCE_COLOR_03", text='').action = 'ASIAN SKIN'
        skin.operator('makebaby.actions', icon="SEQUENCE_COLOR_08", text='').action = 'AFRICAN SKIN'

        ### Eyes
        layout.label(text='Eyes Color: ', icon='HIDE_OFF')
        eyes = layout.row()
        eyes.operator('makebaby.actions', icon="SEQUENCE_COLOR_05", text='').action = 'BLUE EYES'
        eyes.operator('makebaby.actions', icon="COLORSET_08_VEC", text='').action = 'BLUEGREEN EYES'
        eyes.operator('makebaby.actions', icon="SEQUENCE_COLOR_08", text='').action = 'BROWN EYES'
        eyes.operator('makebaby.actions', icon="SEQUENCE_COLOR_02", text='').action = 'BROWNLIGHT EYES'
        eyes.operator('makebaby.actions', icon="COLORSET_06_VEC", text='').action = 'DEEPBLUE EYES'
        eyes.operator('makebaby.actions', icon="SEQUENCE_COLOR_04", text='').action = 'GREEN EYES'
        eyes.operator('makebaby.actions', icon="COLORSET_13_VEC", text='').action = 'GREY EYES'
        eyes.operator('makebaby.actions', icon="SEQUENCE_COLOR_03", text='').action = 'ICE EYES'
        eyes.operator('makebaby.actions', icon="SEQUENCE_COLOR_05", text='').action = 'LIGHTBLUE EYES'

        ### Hair
        layout.label(text='Hair: ', icon='PARTICLEMODE')
        hair = layout.column()

        hair.operator('makebaby.actions', icon="FORCE_VORTEX", text='Afro').action = 'AFRO HAIR'
        hair.operator('makebaby.actions', icon="SPHERECURVE", text='Bob 1').action = 'BOB1 HAIR'
        hair.operator('makebaby.actions', icon="INVERSESQUARECURVE", text='Bob 2').action = 'BOB2 HAIR'
        hair.menu("Braid_dropdown", icon='LIBRARY_DATA_DIRECT', text="Braid")
        hair.operator('makebaby.actions', icon="SMOOTHCURVE", text='Long').action = 'LONG HAIR'
        hair.operator('makebaby.actions', icon="LIGHT_POINT", text='Ponytail').action = 'PONYTAIL HAIR'
        hair.operator('makebaby.actions', icon="CURVES_DATA", text='Short 1').action = 'SHORT1 HAIR'
        hair.operator('makebaby.actions', icon="MOD_OCEAN", text='Short 2').action = 'SHORT2 HAIR'
        hair.operator('makebaby.actions', icon="IPO_EXPO", text='Short 3').action = 'SHORT3 HAIR'
        hair.operator('makebaby.actions', icon="IPO_LINEAR", text='Short 4').action = 'SHORT4 HAIR'
        hair.operator('makebaby.actions', icon="SHADING_RENDERED", text='Bald').action = 'BALD HAIR'

        ### Clothes
        layout.label(text="Clothes: ", icon="MOD_CLOTH")

        ### Export
        export = layout.column()
        export.operator('makebaby.actions', icon="EXPORT", text='Export').action = 'EXPORT'

class Braid_dropdown(bpy.types.Menu):
    bl_label = "Braid Hair Color"

    def draw(self, _context):
        layout = self.layout
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_09", text='default').action = 'BRAID HAIR'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_03", text='ash').action = 'BRAID HAIR ASH'
        layout.operator('makebaby.actions', icon="COLORSET_20_VEC", text='black').action = 'BRAID HAIR BLACK'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_07", text='fuchsia').action = 'BRAID HAIR FUCHSIA'
        layout.operator('makebaby.actions', icon="COLORSET_14_VEC", text='ginger').action = 'BRAID HAIR GINGER'
        layout.operator('makebaby.actions', icon="COLORSET_09_VEC", text='golden').action = 'BRAID HAIR GOLDEN'
        layout.operator('makebaby.actions', icon="COLORSET_13_VEC", text='gray').action = 'BRAID HAIR GRAY'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_01", text='red').action = 'BRAID HAIR RED'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_02", text='strawberry').action = 'BRAID HAIR STRAWBERRY'
        layout.operator('makebaby.actions', icon="COLORSET_07_VEC", text='teal').action = 'BRAID HAIR TEAL'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_06", text='violet').action = 'BRAID HAIR VIOLET'

 
class Actions(bpy.types.Operator):
    bl_idname = 'makebaby.actions'
    bl_label = 'MakeBaby Actions'
    bl_description = 'Operator for the buttons of the UI'
    bl_options = {'REGISTER', 'UNDO'}
 
    action: bpy.props.EnumProperty(
        items=[
            ('CLEAR', 'clear scene', 'clear scene'),
            ('EXPORT', 'Export FBX', 'Export FBX'),
            ('NEW', 'create new avatar', 'create new avatar'),
            ('DELETE', 'delete current avatar', 'delete current avatar'),
            ('CAUCASIAN SKIN', 'change skin to caucasian', 'put caucasian skin'), 
            ('MIDLETON SKIN', 'change skin to midleton', 'put midleton skin'), 
            ('ASIAN SKIN', 'change skin to asian', 'put asian skin'), 
            ('AFRICAN SKIN', 'change skin to african', 'put african skin'), 
            ('AFRO HAIR', 'change hair to affro', 'change hair to afro'),
            ('BOB1 HAIR', 'change hair to bob1', 'change hair to bob1'),
            ('BOB2 HAIR', 'change hair to bob2', 'change hair to bob2'),
            ('BRAID HAIR', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR ASH', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR BLACK', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR FUCHSIA', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR GINGER', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR GOLDEN', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR GRAY', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR RED', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR STRAWBERRY', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR TEAL', 'change hair to braid', 'change hair to braid'),
            ('BRAID HAIR VIOLET', 'change hair to braid', 'change hair to braid'),
            ('LONG HAIR', 'change hair to long', 'change hair to long'),
            ('PONYTAIL HAIR', 'change hair to ponytail', 'change hair to ponytail'),
            ('SHORT1 HAIR', 'change hair to short1', 'change hair to short1'),
            ('SHORT2 HAIR', 'change hair to short2', 'change hair to short2'),
            ('SHORT3 HAIR', 'change hair to short3', 'change hair to short3'),
            ('SHORT4 HAIR', 'change hair to short4', 'change hair to short4'),
            ('BALD HAIR', 'change hair to bald', 'change hair to bald'),
            ('BLUE EYES', 'eyes', 'eyes'),
            ('BLUEGREEN EYES', 'eyes', 'eyes'),
            ('BROWN EYES', 'eyes', 'eyes'),
            ('BROWNLIGHT EYES', 'eyes', 'eyes'),
            ('DEEPBLUE EYES', 'eyes', 'eyes'),
            ('GREEN EYES', 'eyes', 'eyes'),
            ('ICE EYES', 'eyes', 'eyes'),
            ('LIGHTBLUE EYES', 'eyes', 'eyes'),
            ('GREY EYES', 'eyes', 'eyes'),
        ]
    )
                
        
    hair_shape: bpy.props.EnumProperty(
        items=[
            ('afro01', 'afro01', 'afro01'),
            ('bob01', 'bob01', 'bob01'),
            ('bob02', 'bob02', 'bob02'),
            ('braid01', 'braid01', 'braid01'),
            ('long01', 'long01', 'long01'),
            ('ponytail01', 'ponytail01', 'ponytail01'),
            ('short01', 'short01', 'short01'),
            ('short02', 'short02', 'short02'),
            ('short03', 'short03', 'short03'),
            ('short04', 'short04', 'short04'),
            ('bald', 'bald', 'bald'),
        ]

    )
 
    def execute(self, context):
        if self.action == 'CLEAR':
            self.clear_scene(context=context)
        elif self.action == 'EXPORT':
            self.export(context=context)
        elif self.action == 'NEW':
            self.new(self=self, context=context)
        elif self.action == 'DELETE':
        # SKIN
            self.delete(self=self, context=context)
        elif self.action == 'CAUCASIAN SKIN':
            self.skin(self=self, context=context, color='young_caucasian_female')
        elif self.action == 'MIDLETON SKIN':
            self.skin(self=self, context=context, color='callharvey3d_midtoned_female')
        elif self.action == 'ASIAN SKIN':
            self.skin(self=self, context=context, color='young_asian_female')
        elif self.action == 'AFRICAN SKIN':
            self.skin(self=self, context=context, color='middleage_african_female')
        # HAIR
        elif self.action == 'AFRO HAIR':
            self.hair(self=self, context=context, shape='afro01')
        elif self.action == 'BOB1 HAIR':
            self.hair(self=self, context=context, shape='bob01')
        elif self.action == 'BOB2 HAIR':
            self.hair(self=self, context=context, shape='bob02')  

        elif self.action == 'BRAID HAIR':
            self.hair(self=self, context=context, shape='braid01') 
        elif self.action == 'BRAID HAIR ASH':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_ash')
        elif self.action == 'BRAID HAIR BLACK':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_black')
        elif self.action == 'BRAID HAIR FUCHSIA':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_fuchsia')
        elif self.action == 'BRAID HAIR GINGER':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_ginger')
        elif self.action == 'BRAID HAIR GOLDEN':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_golden')
        elif self.action == 'BRAID HAIR GRAY':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_gray')
        elif self.action == 'BRAID HAIR RED':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_red')
        elif self.action == 'BRAID HAIR STRAWBERRY':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_strawberry')
        elif self.action == 'BRAID HAIR TEAL':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_teal')
        elif self.action == 'BRAID HAIR VIOLET':
            self.hair(self=self, context=context, shape='braid01', material='toigo_braid01_violet')

        elif self.action == 'LONG HAIR':
            self.hair(self=self, context=context, shape='long01') 
        elif self.action == 'PONYTAIL HAIR':
            self.hair(self=self, context=context, shape='ponytail01') 
        elif self.action == 'SHORT1 HAIR':
            self.hair(self=self, context=context, shape='short01') 
        elif self.action == 'SHORT2 HAIR':
            self.hair(self=self, context=context, shape='short02') 
        elif self.action == 'SHORT3 HAIR':
            self.hair(self=self, context=context, shape='short03') 
        elif self.action == 'SHORT4 HAIR':
            self.hair(self=self, context=context, shape='short04')   
        elif self.action == 'BALD HAIR':
            self.hair(self=self, context=context, shape='bald')   
        elif self.action == 'BLUE EYES':
            self.eyes(self=self, context=context, color='blue')
        elif self.action == 'BLUEGREEN EYES':
            self.eyes(self=self, context=context, color='bluegreen')
        elif self.action == 'BROWN EYES':
            self.eyes(self=self, context=context, color='brown')
        elif self.action == 'BROWNLIGHT EYES':
            self.eyes(self=self, context=context, color='brownlight')
        elif self.action == 'DEEPBLUE EYES':
            self.eyes(self=self, context=context, color='deepblue')
        elif self.action == 'GREEN EYES':
            self.eyes(self=self, context=context, color='green')
        elif self.action == 'GREY EYES':
            self.eyes(self=self, context=context, color='grey')
        elif self.action == 'ICE EYES':
            self.eyes(self=self, context=context, color='ice')
        elif self.action == 'LIGHTBLUE EYES':
            self.eyes(self=self, context=context, color='lightblue')
            
        return {'FINISHED'}
 
    @staticmethod
    def clear_scene(context):
        for obj in bpy.data.objects:
            bpy.data.objects.remove(obj)
 
    @staticmethod
    def export(context):
            #select everything relevant
            rig = select_rig()
            for obj in rig.children:
                obj.select_set(True)

            bpy.ops.export_scene.fbx(
                #set to assets folder of Unity project
                filepath            = EXPORT_PATH + '/avatar_export.fbx',
                check_existing      = True,

                use_selection       = True,

                path_mode           = 'COPY', 
                embed_textures      = True, 

                axis_forward        = '-Z',
                axis_up             = 'Y',

                global_scale        = 1.0,
                
                primary_bone_axis   = 'X', 
                secondary_bone_axis = 'Z', 
                
                    
            )

    @staticmethod
    def new(self, context):
        # settings for Unity
    
        bpy.context.scene.MPFB_ASLS_skin_type = 'MAKESKIN'
        bpy.context.scene.MPFB_ASLS_material_instances = False
        bpy.context.scene.MPFB_ASLS_procedural_eyes = False


        bpy.ops.mpfb.create_human()

        # set rig
        bpy.context.scene.MPFB_ADR_standard_rig = 'game_engine'
        bpy.ops.mpfb.add_standard_rig()
        
        select_basemesh()

        
        


        # Define the data in the tuple format
        data = [
            (['eyes', 'low-poly'], 'low-poly.mhclo', 'Eyes', 'GAMEENGINE', 'brown.mhmat'),
            (['eyelashes', 'eyelashes01'], 'eyelashes01.mhclo', 'Eyelashes', 'MAKESKIN', ''),
            (['eyebrows', 'eyebrow001'], 'eyebrow001.mhclo', 'Eyebrows', 'MAKESKIN', ''),
            (['hair', 'short01'], 'short01.mhclo', 'Hair', 'MAKESKIN', 'short01.mhmat'),
            (['skins', 'young_caucasian_female'], 'young_caucasian_female.mhmat', 'Skin', '', ''),
            (['clothes', 'shoes04'], 'shoes04.mhclo', 'Clothes' , 'MAKESKIN', 'shoes04.mhmat'),
            (['clothes', 'male_casualsuit02'], 'male_casualsuit02.mhclo', 'Clothes', 'MAKESKIN', 'male_casualsuit02.mhmat'),
        ]
        
        self.hair_shape = 'short01'

        # Load the items
        for folders, file_name, obj_type, mat_type, mat_file in data:
            data_folder_path = DATA_FOLDER
            for folder in folders:
                data_folder_path += SEPARATOR + folder
            file_path = data_folder_path + SEPARATOR + file_name
            if obj_type == 'Skin' :
                bpy.ops.mpfb.load_library_skin(filepath=file_path)
            else:
                bpy.ops.mpfb.load_library_clothes(filepath=file_path, object_type=obj_type, material_type=mat_type)
            if mat_file:
                bpy.context.scene.MPFB_ALTM_available_materials = mat_file
                bpy.ops.mpfb.load_library_material()
        
        

        # select basemesh 
        select_basemesh()

    @staticmethod
    def delete(self, context):
        rig = select_rig()
        for obj in rig.children:
            print(obj)
            obj.select_set(True)
        
        bpy.ops.object.delete(use_global=False)

    @staticmethod
    def skin(self, context, color):
        file_path = DATA_FOLDER + '/skins/' + color + SEPARATOR + color + ".mhmat"
        bpy.ops.mpfb.load_library_skin(filepath=file_path)   

    @staticmethod
    def hair(self, context, shape, material=None):

        # unload previous
        if self.hair_shape != 'bald' :
            unload_path = str(self.hair_shape) + SEPARATOR + str(self.hair_shape) + '.mhclo'
            print(f"UNLOADING : {unload_path}")
            bpy.ops.mpfb.unload_library_clothes(filepath=unload_path)
            select_basemesh()
        
        # load current
        if shape != 'bald': 
            file_path = DATA_FOLDER + '/hair/' + shape + SEPARATOR + shape + ".mhclo"
            bpy.ops.mpfb.load_library_clothes(filepath=file_path, object_type="Hair", material_type="MAKESKIN")

        # load alternative hair color
        if material:
            bpy.context.scene.MPFB_ALTM_available_materials = material + ".mhmat"
            bpy.ops.mpfb.load_library_material()

        # register new shape for next unload
        self.hair_shape = shape

        select_basemesh()

    @staticmethod
    def eyes(self, context, color):
        
        # reload eyes
        bpy.ops.mpfb.unload_library_clothes(filepath="low-poly/low-poly.mhclo")
        eyes_path = DATA_FOLDER + SEPARATOR + 'eyes' + SEPARATOR + 'low-poly' + SEPARATOR + 'low-poly.mhclo'
        bpy.ops.mpfb.load_library_clothes(filepath=eyes_path, object_type="Eyes", material_type="GAMEENGINE")


        bpy.context.scene.MPFB_ALTM_available_materials = color + ".mhmat"
        bpy.ops.mpfb.load_library_material()

        select_basemesh()

def register():
    bpy.utils.register_class(Actions)
    bpy.utils.register_class(Braid_dropdown)
    bpy.utils.register_class(AvatarPanel)

    


def unregister():
    bpy.utils.unregister_class(Actions)
    bpy.utils.unregister_class(Braid_dropdown)
    bpy.utils.unregister_class(AvatarPanel)


if __name__ == "__main__":
    register()
