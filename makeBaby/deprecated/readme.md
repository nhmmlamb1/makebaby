# makeBaby is a fork of the following program :
https://github.com/makehumancommunity/mpfb2

# differences with makeHuman :
makeBaby is meant to be used by people who are no 3D experts
makeHuman is for people who have good basic knowledge of 3D, blender and understand things like rigging, texturing, modeling
when making a 3D character a certain order has to be respected : first the gemometry, then the rig then the clothes...
This order makes sense but is not intuitive to beginner users

