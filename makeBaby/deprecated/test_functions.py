import numpy as np
import matplotlib.pyplot as plt

# Define the input values
x = np.linspace(0, 1, 1000)

# Define the three sinusoidal functions
muscle = 0.5 + 0.5*np.sin(4 * np.pi * x)
weight = 0.5 + 0.5*np.sin(8 * np.pi * x)
proportions = 0.5 + 0.5 * np.sin(2 * np.pi * x)

# Plot the functions
plt.figure(figsize=(10, 6))
plt.plot(x, muscle, label='muscle')
plt.plot(x, weight, label='weight')
plt.plot(x, proportions, label='proportions')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Sinusoidal Functions')
plt.legend()
plt.grid(True)
plt.show()
