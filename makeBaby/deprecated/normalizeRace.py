import bpy

class NormalizeRace(bpy.types.Operator):
    bl_idname = "makebaby.normalize_race"
    bl_label = "Normalize Race"
    bl_description = """Normalize Ethnicity Components 
    (components are normalized if African + Asian + Caucasian = 1)"""

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        total_sum = (
            bpy.context.scene.mpfb_macropanel_african
            + bpy.context.scene.mpfb_macropanel_asian
            + bpy.context.scene.mpfb_macropanel_caucasian
        )
        
        if total_sum != 0:
            bpy.context.scene.mpfb_macropanel_african /= total_sum
            bpy.context.scene.mpfb_macropanel_asian /= total_sum
            bpy.context.scene.mpfb_macropanel_caucasian /= total_sum
        else:
            bpy.context.scene.mpfb_macropanel_african = 1/3
            bpy.context.scene.mpfb_macropanel_asian = 1/3
            bpy.context.scene.mpfb_macropanel_caucasian = 1/3
        return {'FINISHED'}


def menu_func(self, context):
    self.layout.operator(NormalizeRace.bl_idname, text=NormalizeRace.bl_label)

def register():
    bpy.utils.register_class(NormalizeRace)
    bpy.types.VIEW3D_MT_object.append(menu_func)


def unregister():
    bpy.utils.unregister_class(NormalizeRace)
    bpy.types.VIEW3D_MT_object.remove(menu_func)


if __name__ == "__main__":
    register()