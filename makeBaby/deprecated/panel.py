import bpy
from bpy.types import (Panel, Operator)
from mpfb2.mpfb.services.locationservice import LocationService
# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

class SimpleOperator(Operator):
    """Print object name in Console"""
    bl_idname = "object.simple_operator"
    bl_label = "Simple Object Operator"

    def execute(self, context):
        print (context.object)
        return {'FINISHED'}


class CameraSetup(Operator):
    """Prepare the scene"""
    bl_idname = "object.camera_setup"
    bl_label = "Camera Setup"

    def execute(self, context):
        scn = context.scene

        # delete all objects
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.object.delete(use_global=False)
        # create the first camera
        cam1 = bpy.data.cameras.new("Camera 1")
        cam1.lens = 18

        # create the first camera object
        cam_obj1 = bpy.data.objects.new("Camera 1", cam1)
        cam_obj1.location = (9.69, -10.85, 12.388)
        cam_obj1.rotation_euler = (0.6799, 0, 0.8254)
        scn.collection.objects.link(cam_obj1)
        bpy.ops.view3d.object_as_camera(cam_obj1)

        return {'FINISHED'}
# ------------------------------------------------------------------------
#    Panel in Object Mode
# ------------------------------------------------------------------------

class BabyPanel(Panel):
    bl_idname = "object.baby_panel"
    bl_label = "Avatar customization"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "UI"
    bl_category = "BabyPanel"
    bl_context = "objectmode"   


    @classmethod
    def poll(self,context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object

        layout.label(text="Properties:")

        col = layout.column(align=True)
        row = col.row(align=True)
        row.prop(obj, "show_name", toggle=True, icon="FILE_FONT")
        row.prop(obj, "show_wire", toggle=True, text="Wireframe", icon="SHADING_WIRE")
        col.prop(obj, "show_all_edges", toggle=True, text="Show all Edges", icon="MOD_EDGESPLIT")
        layout.separator()

        layout.label(text="Operators:")

        col = layout.column(align=True)
        col.operator(CameraSetup.bl_idname, text="Setup the camera", icon="CONSOLE")

        layout.separator()

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_class(BabyPanel)
    bpy.utils.register_class(CameraSetup)
    

def unregister():
    bpy.utils.unregister_class(BabyPanel)
    bpy.utils.unregister_class(CameraSetup)

if __name__ == "__main__":
    register()
