import bpy


class HelloWorldPanel(bpy.types.Panel):
    bl_idname = "OBJECT_PT_hello_world"
    bl_label = "Hello World"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"

    def draw(self, context):
        self.layout.label(text="Hello World")


bpy.utils.register_class(HelloWorldPanel)




import bpy


class HelloWorldPanel(bpy.types.Panel):
    bl_idname = "OBJECT_PT_hello_world"
    bl_label = "Hello World"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = "object"

    def draw(self, context):
        self.layout.label(text="Hello World")


bpy.utils.register_class(HelloWorldPanel)




    Define a BoolProperty for a 'Checkbox'
    Define a FloatProperty or IntProperty to get a 'Slider'
    Define a StringProperty for all different kinds of 'Character Input' or 'File Paths'
    Define an EnumProperty to get a 'Dropdown Menu' or 'Radio Selection'
    Define a FloatVectorProperty or IntVectorProperty for compound values eg. 'Color Pickers', 'Location Coordinates', 'Velocity Vectors', 'Matrices' etc. 