import bpy
import sys
import os

dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir )
    
    
print("dir : " + dir)

#from testImport import test1

# this next part forces a reload in case you edit the source after you first start the blender session
# import imp
# imp.reload(testImport)

#from test import test1
#from mpfb2.mpfb.services.humanservice import HumanService

# print(HumanService.get_list_of_human_presets())
