
Python
import bpy
from bpy.props import EnumProperty
from bpy.types import Operator, Panel
from bpy.utils import register_class, unregister_class
 
class TEST_PT_panel(Panel):
    bl_idname = 'TEST_PT_panel'
    bl_label = 'Test'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Test'
 
    def draw(self, context):
        layout = self.layout
        layout.operator('test.test_op', text='Clear scene').action = 'CLEAR'
        layout.operator('test.test_op', text='Add cube').action = 'ADD_CUBE'
        layout.operator('test.test_op', text='Add sphere').action = 'ADD_SPHERE'
 
 
class TEST_OT_test_op(Operator):
    bl_idname = 'test.test_op'
    bl_label = 'Test'
    bl_description = 'Test'
    bl_options = {'REGISTER', 'UNDO'}
 
    action: EnumProperty(
        items=[
            ('CLEAR', 'clear scene', 'clear scene'),
            ('ADD_CUBE', 'add cube', 'add cube'),
            ('ADD_SPHERE', 'add sphere', 'add sphere')
        ]
    )
 
    def execute(self, context):
        if self.action == 'CLEAR':
            self.clear_scene(context=context)
        elif self.action == 'ADD_CUBE':
            self.add_cube(context=context)
        elif self.action == 'ADD_SPHERE':
            self.add_sphere(context=context)
        return {'FINISHED'}
 
    @staticmethod
    def clear_scene(context):
        for obj in bpy.data.objects:
            bpy.data.objects.remove(obj)
 
    @staticmethod
    def add_cube(context):
        bpy.ops.mesh.primitive_cube_add()
 
    @staticmethod
    def add_sphere(context):
        bpy.ops.mesh.primitive_uv_sphere_add()
 
def register():
    register_class(TEST_OT_test_op)
    register_class(TEST_PT_panel)