import bpy

def select_basemesh():
    basemesh = bpy.context.scene.objects["Human"]       # Get the object
    bpy.ops.object.select_all(action='DESELECT')        # Deselect all objects
    bpy.context.view_layer.objects.active = basemesh    # Make the cube the active object 
    basemesh.select_set(True)
    return basemesh
    
def select_rig():
    rig = bpy.context.scene.objects["Human.rig"]              # Get the object
    bpy.ops.object.select_all(action='DESELECT')        # Deselect all objects
    bpy.context.view_layer.objects.active = rig         # Make the cube the active object 
    rig.select_set(True)
    return rig

'''
creates a material named @mat_name and asign it @context.active_object
material is a colorized @image using color of @prop 
'''
def colorise_material(context, mat_name, image, prop, mat_adjust):
    obj = bpy.context.active_object
    mat = bpy.data.materials.get(mat_name)
    if mat is None:
        mat = bpy.data.materials.new(name=mat_name)
    mat.use_nodes = True
    nodes = mat.node_tree.nodes
    diffuse = nodes.new('ShaderNodeTexImage')
    diffuse.location = (-600, -100)
    diffuse.image = bpy.data.images.load(image)
    hsv = nodes.new("ShaderNodeHueSaturation")
    hsv.location = (-300,250)
    addHue = nodes.new("ShaderNodeMath")
    addHue.location = (-570, 300)
    addHue.inputs[1].default_value = mat_adjust
    converter = nodes.new("ShaderNodeSeparateColor")
    converter.location = (-700, 200)
    converter.mode = 'HSV'
    converter.inputs["Color"].default_value = prop
    p_BSDF = nodes["Principled BSDF"]
    mat.node_tree.links.new(converter.outputs[0], addHue.inputs[0])
    mat.node_tree.links.new(addHue.outputs[0], hsv.inputs['Hue'])
    mat.node_tree.links.new(converter.outputs[1], hsv.inputs['Saturation'])
    mat.node_tree.links.new(converter.outputs[2], hsv.inputs['Value'])
    mat.node_tree.links.new(hsv.outputs['Color'], p_BSDF.inputs['Base Color'])
    mat.node_tree.links.new(diffuse.outputs['Color'], hsv.inputs['Color'])
    if obj.data.materials:
        obj.data.materials[0] = mat
    else:
        obj.data.materials.append(mat)
    select_basemesh()