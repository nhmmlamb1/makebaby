
class BraidDropdown(bpy.types.Menu):
    bl_label = "Braid Hair Color"

    def draw(self, _context):
        layout = self.layout
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_09", text='default').action = 'BRAID HAIR'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_03", text='ash').action = 'BRAID HAIR ASH'
        layout.operator('makebaby.actions', icon="COLORSET_20_VEC", text='black').action = 'BRAID HAIR BLACK'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_07", text='fuchsia').action = 'BRAID HAIR FUCHSIA'
        layout.operator('makebaby.actions', icon="COLORSET_14_VEC", text='ginger').action = 'BRAID HAIR GINGER'
        layout.operator('makebaby.actions', icon="COLORSET_09_VEC", text='golden').action = 'BRAID HAIR GOLDEN'
        layout.operator('makebaby.actions', icon="COLORSET_13_VEC", text='gray').action = 'BRAID HAIR GRAY'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_01", text='red').action = 'BRAID HAIR RED'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_02", text='strawberry').action = 'BRAID HAIR STRAWBERRY'
        layout.operator('makebaby.actions', icon="COLORSET_07_VEC", text='teal').action = 'BRAID HAIR TEAL'
        layout.operator('makebaby.actions', icon="SEQUENCE_COLOR_06", text='violet').action = 'BRAID HAIR VIOLET'

 