# -----------------------------------------------------------------------------------------------
# stupid path stuff 


import os
import sys
OS = "linux"
# OS = "windows"
if OS == "linux":      
    CODE_PATH   = os.getcwd() + '/BA6/avatar/vr-hero/makeBaby/'  
elif OS == "windows":
    CODE_PATH   = "C:/Users/shvr_ura/Documents/" # TODO : find full path
sys.path.append(CODE_PATH)

# -----------------------------------------------------------------------------------------------

import bpy
import properties as prop
import actions as act
import avatarPanel as ava

bpy.types.Scene.makebaby_gender      = bpy.props.IntProperty(name="Body Gender Presentation"  , min=0, max=6, default=3 , update=prop.update_gender)

bpy.types.Scene.makebaby_weight      = bpy.props.IntProperty(name="Weight"             , min=0, max=6, default=3 , update=prop.update_weight)
bpy.types.Scene.makebaby_muscle      = bpy.props.IntProperty(name="Muscle"             , min=0, max=6, default=3 , update=prop.update_muscle)
bpy.types.Scene.makebaby_proportions = bpy.props.IntProperty(name="Shoulders"           , min=0, max=6, default=3 , update=prop.update_proportions)

bpy.types.Scene.makebaby_race  = bpy.props.FloatVectorProperty(name    ="Race", subtype='COLOR',size = 4,
                                                                    min     = 0.0,
                                                                    max     = 10.0,
                                                                    default = (0.01, 0.01, 0.01, 1),
                                                                    update  = prop.update_race)    


bpy.types.Scene.makebaby_age         = bpy.props.IntProperty(name="Age"             , min=0, max=6, default=3, update=prop.update_age)

bpy.types.Scene.makebaby_skin_color  = bpy.props.FloatVectorProperty(name    ="Skin Color", subtype='COLOR',size = 4,
                                                                    min     = 0.0,
                                                                    max     = 1.0,
                                                                    default = (0.25295, 0.0807507, 0.0221213, 1),
                                                                    update  = prop.update_skin_color)

bpy.types.Scene.makebaby_eyes_color  = bpy.props.FloatVectorProperty(name    ="Eyes Color", subtype='COLOR',size = 4,
                                                                    min     = 0.0,
                                                                    max     = 1.0,
                                                                    default = (0.1, 0.8, 0.0221213, 1),
                                                                    update  = prop.update_eyes_color)          

bpy.types.Scene.makebaby_hair_color  = bpy.props.FloatVectorProperty(name    ="Hair Color", subtype='COLOR',size = 4,
                                                                    min     = 0.0,
                                                                    max     = 1.0,
                                                                    default = (0, 0.0191363, 0.0396813, 1),
                                                                    update  = prop.update_hair_color)                                                                                                                                



def register():
    act.register()
    ava.register()

    bpy.types.Scene.conf_path = bpy.props.StringProperty \
      (
      name = "Path",
      default = "",
      description = "Define the export path of the project",
      subtype = 'DIR_PATH'
      )


def unregister():
    actions.unregister()
    ava.unregister()
    del bpy.types.Scene.conf_path


if __name__ == "__main__":
    register()