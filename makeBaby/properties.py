import bpy
from math import sin, pi, radians
from selectUtils import select_basemesh

import os
OS = "linux"
# OS = "windows"

if OS == "linux":    
    DATA_FOLDER = '/home/leanahtan/.config/blender/4.0/mpfb/data'
    EXPORT_PATH = os.getcwd() + '/BA6/avatar/vr-hero/makeBaby'
    SEPARATOR   = "/"
elif OS == "windows":
    DATA_FOLDER = "C:/Users/shvr_ura/AppData/Roaming/Blender Foundation/Blender/4.0/mpfb/data"
    EXPORT_PATH = "C:/Users/shvr_ura/Documents/shvr-main/SHVRUnity/Assets/chars"
    SEPARATOR   = "/"  


# helper methods : -----------------------------
 

def adapt_height(context):
    gender = context.scene.mpfb_macropanel_gender
    asian = context.scene.mpfb_macropanel_asian
    caucasian = context.scene.mpfb_macropanel_caucasian

    context.scene.mpfb_macropanel_height = (
        0.5
         - 0.150*gender
         + 0.060*caucasian
         + 0.138*asian
    )

# update properties methods : 

def update_gender(self, context):
    select_basemesh()
    def smoothstep(x):
        return x * x * (3.0 - 2.0 * x)
    shape = context.scene.makebaby_gender / 6
    context.scene.mpfb_macropanel_gender = shape
    context.scene.mpfb_macropanel_cupsize = 0.75 - 0.25*smoothstep(shape)
    adapt_height(context)
    bpy.ops.mpfb.refit_human()

def update_weight(self, context):
    select_basemesh()
    context.scene.mpfb_macropanel_weight = context.scene.makebaby_weight / 6
    bpy.ops.mpfb.refit_human()   

def update_muscle(self, context):
    select_basemesh()
    context.scene.mpfb_macropanel_muscle = context.scene.makebaby_muscle / 6
    bpy.ops.mpfb.refit_human() 

def update_proportions(self, context):
    select_basemesh()
    context.scene.mpfb_macropanel_proportions = context.scene.makebaby_proportions / 6
    bpy.ops.mpfb.refit_human()  

def update_race(self, context):
    r, g, b, a = context.scene.makebaby_race
    rgb = r + g + b
    context.scene.mpfb_macropanel_caucasian = r / rgb
    context.scene.mpfb_macropanel_asian = g / rgb
    context.scene.mpfb_macropanel_african = b / rgb
    print(f"r+g+b={rgb}")
    print(f"race={r / rgb + g / rgb + b / rgb}")
    adapt_height(context)
    bpy.ops.mpfb.refit_human()


def update_age(self, context):
    age = (context.scene.makebaby_age *0.5 * 1/6) + 0.5
    context.scene.mpfb_macropanel_age = age
    bpy.ops.mpfb.refit_human()

def update_skin_color(self, context):

    nodes = bpy.data.materials['Skin'].node_tree.nodes


    converter = nodes.get("Separate Color")

    converter.inputs["Color"].default_value = context.scene.makebaby_skin_color

    # select basemesh 
    select_basemesh()

def update_eyes_color(self, context):
    eyes = bpy.context.scene.objects["Human.eyes"]
    bpy.ops.object.select_all(action='DESELECT')   
    bpy.context.view_layer.objects.active = eyes    
    eyes.select_set(True)

    nodes = eyes.data.materials['Eyes'].node_tree.nodes


    property_color = nodes.get("Separate Color")
    property_color.inputs["Color"].default_value = context.scene.makebaby_eyes_color

    # select basemesh 
    select_basemesh()

def update_hair_color(self, context):
    hair = bpy.context.scene.objects["Human.hair"]
    bpy.ops.object.select_all(action='DESELECT')   
    bpy.context.view_layer.objects.active = hair    
    hair.select_set(True)

    # TODO : apparently hair is only for the short
    nodes = hair.active_material.node_tree.nodes
    nodes["diffuseIntensity"].inputs[1].default_value = context.scene.makebaby_hair_color

    # select basemesh 
    select_basemesh()


# def update_morpho(self, context):
#     morpho = context.scene.makebaby_morphology / 10
#     context.scene.mpfb_macropanel_muscle      = 0.5 + 0.5*sin(2*pi*morpho)
#     context.scene.mpfb_macropanel_proportions = 0.5 + 0.5*sin(8*pi*morpho)
#     context.scene.mpfb_macropanel_weight      = 0.5 + 0.5*sin(4*pi*morpho)
#     bpy.ops.mpfb.refit_human()