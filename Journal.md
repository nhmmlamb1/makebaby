# Pipeline automations :
| step removed  |  work description | estimated cost (hours) |
| ------------- | ----------------- | ---------------------- |
| 1,2, 15 | have a python script to open blender with correct viewport and scene objects | 5h |
| 3, 7, 8, 9 | make an other addon that interfaces with makehuman | 20h |
| 9,10,11 | create a blender command to export | 2h |
| 12, 13, 14 | save export parameters as preset | 1h |
| 16, 17, 18, 19,20,21,22 | ?? see more about unity | ?? |

# Pipeline :
0. VR setup
1. open blender + delete all
2. open MPFB addon tab
3. apply assets > skin type : model v0 + 2 unbox 
4. adjust slider
5. create mesh
6. adjust mesh
7. game engines rig 
8. add low poly eyes, skin, hair, clothes
9. operations : delete helpers + bake
10. select hierarchy
11. export fbx
12. limit to selected
13. copy path + embeded checkbox
14. save in assets folder + name
15. open unity
16. extract materials and textures
17. for each material set albedo to correct texture image + transparent
18. set rig to humanoid
19. disable previous character
20. drag new character + set location to 0,0,0
21. drag character in the AvatarControl>AutoIKBones reference
22. check + uncheck the box
23. run unity

# At the lab :
- brute force bone orientation parameters
- import textures + normal map
- automate pipeline 15-22

# At home :
- write code to load skin in blender + nodes with RGB property
- write code to bake texture and set in FBX
- same for eye color
- same for skin color
- religious clothing



# week 11 +
- [x] (8) Figuring out joint angle mapping for VR
- [x] (8) Skin Colour
- [x] (3) Age
- [x] (3) Shirt texture
- [x] (8) Eye Color
- [x] (4) Eyebrows
- [x] (6) Hairstyles
- [x] (3) make eye color like this : ![Alt text](image.png)

- [x] (6) Glasses
- [x] (3) Fix this BUg : RuntimeError: Error: Python: Traceback (most recent call last):
  File "/home/leanahtan/.config/blender/4.1/scripts/addons/mpfb/ui/model/operators/refithuman.py", line 20, in execute
    HumanService.refit(blender_object)
  File "/home/leanahtan/.config/blender/4.1/scripts/addons/mpfb/services/humanservice.py", line 1248, in refit
    ClothesService.fit_clothes_to_human(child, basemesh, set_parent=False)
  File "/home/leanahtan/.config/blender/4.1/scripts/addons/mpfb/services/clothesservice.py", line 133, in fit_clothes_to_human
    raise IOError(mhclo_fragment + " does not exist")
OSError: kwnet_at_optical_glasses/kwnet_at_optical_glasses.mhclo does not exist
Location: /usr/share/blender/4.1/scripts/modules/bpy/ops.py:109
- [x] (8) Idea to fix the previous bug : make an asset pack with all the clothes needed yes OR at least : clothe needs to be loaded into the library for refit to work
- [x] (8) Facial Hair
- [x] (8) Religious Clothing : https://sketchfab.com/3d-models/hijab-ee50e01adc864ccc880caed9b5eb3bcb
- [x] (5) find a way to make clothes : body vertex group on clothes, delete group on human

- [x] () low value color picker for race
- [x] (1) default values of UI in new
- [x] (8) discrete sliders fit race issue 

- [ ] (4) find other assets turban, taqiyah cap (MS Rocketbox), hijab (MS Rocketbox), kippah, niqab

- [x]() thumbnails : RIP : https://devtalk.blender.org/t/allow-scaling-of-icons-for-operators-or-properties-in-the-ui/20351

- [ ] (8) Hair Color + Facial HAir Color
- [ ] () make a script node : set sample seed ,loop on sample pixel 100x loop (non black, non transparent) average r with r, g with g, b with b, extract hue for offset

- [ ] (10) Facial appearance

- [ ] (6) Bake textures for export
- [x] (5) figure out paths

- [ ] (6) make it a package

- [ ] (10) Unity automate script import

- [ ] (16) accesibility body change ?
- [ ] (16) accesibility Devices ?
- [ ] (32) linking blender unity ?


# Week 10

random cool stuff to generate 3D stuff with code : 
https://blenderartists.org/t/adding-material-properties-in-blender-using-python/1335242

usefull for materials :
https://devtalk.blender.org/t/how-to-connect-nodes-using-script-commands/11567/12
https://blender.stackexchange.com/questions/23433/how-to-assign-a-new-material-to-an-object-in-the-scene-from-python
https://blender.stackexchange.com/questions/191091/bake-a-texture-map-with-python

skin tones :
https://preview.redd.it/0a3a8wyn0gv71.png?width=840&format=png&auto=webp&s=beebffef4e47fa1b958b7dae985b456fe3b77c3d

- start with darkest skin color (baboon 2) 
- mix color add color RGB 
- bake into color texture (export)

idea: instead of embedding textures in FBX, bake them in asset folder

in unity : 
- load texture + normal map
make sure that : AvatarControl>VR IK>Fix Transforms is checked

instead of baboon with color ranp use white skin -> hsl -> bsdf

origin for Gogo is fixed evem when I move : fixed

In unity :
Hierarchy > Meta > Avatar Control > Inspector :
- Avatar Auto IK Bones (Script) > Avatar > **avatar_export**
- Go Go (Script) > Right Shoulder > **clavicle_r**
- Go Go (Script) > Left Shoulder > **clavicle_l**
- Superhero VRIK Callibration Controller (Script) > Shoulder > **clavicle_r**
- Superhero VRIK Callibration Controller (Script) > Upper Arm > **upperarm_r**
- Superhero VRIK Callibration Controller (Script) > Elbow > **lowerarm_r**
- Superhero VRIK Callibration Controller (Script) > Hand > **hand_r**

# Week 8 :
| hours | cumul | task |
|-------|-------|------|
| 4h    | 4h    | reverse engineering height linear approximation to cancel bone rotation hanging global height of avatar |
| 1h    | 5h    | blender has no support for property, makebaby cannot update it's ui if things are changed in mpfb |
| 1h    | 6h    | https://b3d.interplanety.org/en/calling-functions-by-pressing-buttons-in-blender-custom-ui/ TODO for buttons |
| 2h    | 8h    | auto normalize of race need to go to the lab to recover export code  TODO: addskin |
| 1h    | 9h    | redo export and add Actions Operator |
| 4h    | 13h   | trying to add clothes but mpfb is a bit annoying with textures for some reason |

# Week 7 :
| hours | cumul | task |
|-------|-------|------|
| 4h    | 4h    | trying to fix the race 3 sliders to sum up to 1. issues with properties : add mutex to avoid recursive call of update, min, max, description |
| 1h   | 5h     | trying to write a normalizeRace operator, but registration is broken |
| 1h   | 6h     | normalize is ok, need body shape next, plus other stuff : skin, clothes, rig ... |
| 2h   | 8h     | why is blender not showing icons ?? I just don't understand |
| 1h   | 9h     | trying to change property gender when property shape is updated.. how to link property ? |
| 1h   | 10h    | planning : add the export file, operator to select hierarchy + export, path selector to assets unity, operator initial steps (use instead of createHuman), find proper function for bodyType(gender + proportions + breast) and BodyShape  (muscle + weight) sliders, add clothes and rig operators, delete human operator |
| 2h   | 12h    | meeting + trying export, no need for a new operatorhttps://blender.stackexchange.com/questions/266287/how-to-install-python-packages-using-the-pip-that-now-comes-with-blender | https://doc.qt.io/qt-6/qabstractslider.html#valueChanged

# Week 6 :
| hours | cumul | task |
|-------|-------|------|
| 2h    | 2h    | messing around wit git and blender versions bpy is useless, just run from blender and import as addon |
| 1h    | 3h    | https://docs.blender.org/api/current/bpy.types.Panel.html tryning space_types |
| 1h    | 4h    | looking for mpfb documentation : mpfb.services is the API I should use but there is no doc unfortunately https://github.com/makehumancommunity/mpfb2/blob/master/docs/mpfb_as_library/using_mpfb_as_a_code_library.md |
| 2h    | 6h    | python package tutorial : https://packaging.python.org/en/latest/tutorials/packaging-projects/ but this is not what I need|
| 1h    | 7h    | in blender scipting : templates, python run or blender script ? For now : Operators + single file UI panel |
| 2h    | 9h    | blender layout + default file + simple operator + background |
| 1h    | 10h   | https://blender.stackexchange.com/questions/181928/does-blender-use-a-python-virtual-environment this might be an issue... It's 3 years old so I don't know if it's still up to date. I'm probably just gonna copy paste the mpfb code in a folder but I'm not sure how I can use it  Ok this might be my way to go : https://blender.stackexchange.com/questions/70740/import-external-py-script | 
| 1h    | 11h   | to see the output of print() when running inside blender (for debugging) linux : run blender from terminal, windows : Window > Toggle System Console https://blender.stackexchange.com/questions/6173/where-does-console-output-go |
| 1h    | 12h   | I guess I just have to read THE doc : https://docs.blender.org/api/current/info_overview.html |
| 2h    | 14h   | ok so if I have the mpfb2 addon enabled I can simply call operators and change properties from my BabyHuman addon. I can create other operators that call mpfb operators and change properties |
| 1h    | 15h   | update property : https://docs.blender.org/api/current/bpy.props.html |

# Week 5 :
| hours | cumul | task |
|-------|-------|------|
| 2h    | 2h    | testing orientations but something is wrong even with the main character, waste of time |
| 2h    | 4h    | finding orientation: armature : primary -x secondary z. Figuring out pipeline end |
| 2h    | 6h    | which blender python venv ... |
| 1h    | 7h    | exploring MPFB source code |
| 2h    | 9h    | recording operations in blender with console |
| 4h    | 13h   | unity + input system with Matthew : uncheck disable wheninactive in steamVR setup |
# Week 4 :
| hours | cumul | task |
| ----- | ----- | ---- |
| 1h    | 1h    | download mpfb2 source code, read tutorial on how to make a blender add-on: https://docs.blender.org/manual/en/latest/advanced/scripting/addon_tutorial.html |
| 1h    |  2h   | organisation of week |
| 1h    |  3h   | simple addon made, diving in the code of mpfb2 |
| 1h    |  4h   | playing around with bpy and mpfb2 exploration |
| 1h    |  5h   | blender addons is easy to customize because of good documentations. Let's do Babyhuman. Next step: export from MPFB2 to Unity test IK with VR set |
| 2h    |  7h   | reattempting blender -> unity export https://www.youtube.com/watch?v=yloupOUjMOA https://www.youtube.com/watch?v=sZ8lvoUtGYg |
| 1h    | 8h    | looked at baking, need a better understanding of unity need to bake entire texture in one albedo file + one normal map + merge parts in one mesh with correct unwrap |
| 1h    | 9h    | export succesful albedo |
| 2h    | 11h   | wrestling with windows to spawn subprocess |
| 1h    | 12h   | https://www.youtube.com/watch?v=wG6ON8wZYLc might be interesting. think about pipeline. check unity change visual of character |
| 4h    | 16 h  | testing in vr with bones yx and yy broken wrist |

# Week 3 :
| hours | cumul | task |
| ----- | ----- | ---- |
| 1h    | 1h    | blender MPFB2 tests and avoid crash : use cycles in material preview mode |
| 2h    | 3h    | export to unity test : broken need this tutorial : https://www.youtube.com/watch?v=YypDcUpON8A actually the orientation is not good and the export is ;ore complicqted, let's stick with makehuman grandpa|
| 1h    | 4h    | watch some Makeclothes tutorials |
| 2h    | 6h    | modeling hijab https://www.youtube.com/watch?v=SY17YJHMMMQ |
| 1h    | 7h    | sculpting hijab |
| 1h    | 8h    | exporting hijab to makehuman no material some geometry is broken, not worth it? at least simplify it next time |
| 4h    | 12h   | planing, reviewing options  |
| 2h    | 14h   | meeting, testing and decision MPFB2 vs MakeHuman : which UI is easier to customize ? |
| 1h    | 15h   | exploring the makehuman source code |
| 1h    | 16h   | made some modifications to the Make Human gui : removed age slider |

# comparisons :
## legend :
1/4 : Terrible
2/4 : Poor
3/4 : Good Enough
4/4 : Perfect

|         |  MakeHuman  |  MPFB2  |
| ------- | ----------- | ------- |
| simplicity | 4/4 | 3/4 |
| assets support | 3/4 | 3/4 |
| export to unity | 4/4 | 1/4 |
| customizability | 2/4? | 2/4? | 
| durability | 2/4 | 3/4 | 
| evolvability | 1/4 | 3/4 |


# Week 2 :
| hours | cumul | task |
| ----- | ----- | ---- |
|  1h   |  1h   | weekly review |
|  2h   |  3h   | trying to download assets + tutorials to debug| 
|  1h   |  4h   | getting help on the forum troubleshooting clothes not showing http://www.makehumancommunity.org/forum |
|  1h   |  5h   | questioning MakeHuman : issues: support slow to answer, bugs with clothes, gui only = painful automation |
|  1h   |  6h   | meeting and debugging clothes + plan tasks |
|  2h   |  8h   | testing clothes : skin inside... |
|  1h   |  9h   | (.dae : Y up face Z, bones along local X) fixing orientation of bones |
|  1h   |  10h  | looking for blender add-ons, make clothes |
|  2h   |  12h  | discovering MPFB2, remplace MakeHuman by this ? watching tutorials https://www.youtube.com/@MakeHumanCommunity/featured|
|  4h   |  16h  | installing and testing MPFB2 = better but maybe overkill, blender crashes (4.0 on my laptop) need to test export |


# Week 1 :

| hours | cumul | task |
| ----- | ----- | ---- |
| 2h 30 | 2:30  |  understanding the project requirements and specifics    |
|  1h   | 3:30  | scheduling meetings      |
| 1h    | 4:30  | paying around with make Humans : impressions : looks very creepy, no clothes ? |
| 1h    | 5:30  | intro to unity and project description |
| 3h    | 8:30  | fob + setup + clarifying goals |
| 1h    | 9:30  | setup unity and steam |
| 1h    | 10:30 | attempt to export from MakeHuman to unity |
| 1h    | 11:30 | wifi troubleshoot and basic setup |
| 1h    | 12:30 | following tutorial : https://forum.unity.com/threads/makehuman-and-unity.617311/ |
| 1h    | 13:30 | issue with blender addons cannot import |
| 1h    | 14:30 | trying a different  tutorial : http://www.makehumancommunity.org/wiki/Documentation:Saving_models_for_Unity_and_how_to_import_them_there stuck at changing shader to legacy|
| 1h    | 15:30  | testing mobility with low quality material works fine|
| 0h30  | 16:00  | should I keep make human ? http://www.makehumancommunity.org/forum/viewtopic.php?f=9&t=12734 |